import { useRef, useState, useEffect } from "react";
import Button from "../../components/Button";
import Modal from "../../components/Modal";
import ProductCard from "../../components/productCard/ProductCard"

function Home({updateAddedToCartProductsCount, updateSelectedProductsCount}) {
    const [isAddModalOpen, setIsAddModalOpen] = useState(false);
    const [products, setProducts] = useState([]);
    const selectedProductsId = useRef([]);
    const addedToCartProductsId = useRef([]);

    function handleAddToCart(){
        closeModal();
        console.log(addedToCartProductsId);
        let alreadyAddedToCartProducts = [];

        if (localStorage.getItem('addedToCartProducts')) {
            alreadyAddedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
            if(alreadyAddedToCartProducts.includes(addedToCartProductsId.current)) {
                alert('You`ve already added this product to a cart!');
                return
            }
        }
        localStorage.setItem('addedToCartProducts', JSON.stringify([...alreadyAddedToCartProducts, addedToCartProductsId.current]));
        updateAddedToCartProductsCount();
    }

    useEffect(()=>{
        let selectedProductsFromStorage = [];
        if(localStorage.getItem('selectedProducts')){
            selectedProductsFromStorage = JSON.parse(localStorage.getItem('selectedProducts'));
        }
        console.log('Функция selectedProductsFromStorage сработала');
        selectedProductsId.current = selectedProductsFromStorage;
        console.log(selectedProductsFromStorage);
        console.log(selectedProductsId.current);
    }, [])

    const scrollY = useRef(0); 

    function openAddModal(event, id){
        scrollY.current = window.scrollY;
        setIsAddModalOpen(true);
        document.body.style.position = 'fixed';
        document.body.style.width = '100%';
        document.body.style.boxSizing = 'border-box';
        document.body.style.top = `-${scrollY.current}px`;
        document.body.style.paddingRight = '17px';

        addedToCartProductsId.current = id;
        console.log(addedToCartProductsId.current);
    }

    function closeModal(){
        setIsAddModalOpen(false);
        document.body.style.position = '';
        document.body.style.top = ``;
        document.body.style.width = '';
        document.body.style.boxSizing = '';
        document.body.style.top = `-${scrollY.current}px`;
        window.scrollTo(0, scrollY.current);
        document.body.style.paddingRight = '';
    }

    useEffect(()=>{
        fetch('products.json')
        .then(res => res.json())
        .then(data => setProducts(data.products))
        .catch(error => console.log('An error occured while fetching all products: ', error))
    }, [])

    return (
        <div className="cardsWrapper">
                {products.map((product) => <ProductCard
                                                key={product.setNumber}
                                                updateSelectedProductsCount={updateSelectedProductsCount}
                                                selectedProductsId={selectedProductsId.current}
                                                id={product.id}
                                                name={product.name}
                                                price={product.price}
                                                description={product.description}
                                                color={product.color}
                                                imgUrl={product.imgUrl}
                                                openAddModal={openAddModal}
                                            />)}

            {isAddModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #348a0c 33%, #269618 24% 100%)'
                header='Do you want to add this product to your cart?'
                closeButton='true'
                text='If you choose No you, you can still add it later'
                actions={<><button className='buttonForAddModal' onClick={handleAddToCart}>Add</button>
                <button className='buttonForAddModal' onClick={closeModal}>No</button></>}
                closeModal={closeModal}
            />
            }
            
            <div className="modalButtonsWrapper" style={{display: 'none'}}>
                <Button
                    backgroundColor='#E74C3D'
                    text='Open first modal'и
                />
                <Button
                    backgroundColor='#269618'
                    text='Open second modal'
                    onClick={openAddModal}
                />
            </div>
        </div>
    );
}

export default Home;