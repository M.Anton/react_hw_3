import { useEffect, useRef, useState } from 'react';
import styles from '../../components/productCard/ProductCard.module.scss';
import PropTypes from 'prop-types';
import ProductCard from '../../components/productCard/ProductCard';

function Favorites({updateSelectedProductsCount}){
    let [selectedProducts, setSelectedProducts] = useState([]);
    let [noSelectedProducts, setNoSelectedProducts] = useState(false)
    const selectedProductsIdArray = useRef([]);

    function deleteSelectedProduct(id) {
        let updatedSelectedProductsId = [...selectedProductsIdArray.current].filter(productId => productId!==id);
        localStorage.setItem('selectedProducts', JSON.stringify(updatedSelectedProductsId));
        let updatedSelectedProducts = selectedProducts.filter(product => product.id !== id);
        setSelectedProducts(updatedSelectedProducts);
        updateSelectedProductsCount();
    }

    useEffect(()=>{
        let selectedProductsId = JSON.parse(localStorage.getItem('selectedProducts'));
        selectedProductsIdArray.current = selectedProductsId;
        if(selectedProductsId.length === 0){
            setNoSelectedProducts(true)
        }
        console.log("SELECTED PRODUCTS ID: ", selectedProductsIdArray.current);
    }, [selectedProducts])

    useEffect(()=>{
        fetch('products.json')
        .then(res => res.json())
        .then(data => {
            setSelectedProducts([...data.products].filter(product=>
                selectedProductsIdArray.current.includes(product.id)
            ))
        })
        .catch(error => console.log('An error occured while fetching selected products: ', error))
    }, [])

    return (
        <div className="cardsWrapper">
            {/* При noSelectedProducts === true відображаємо надпис що немає товарів*/}
            {noSelectedProducts 
            ?   <h1 className='noProductsTitle'>There is no selected products yet</h1> 
            :   null 
            }
            {selectedProducts.map(product => <ProductCard
                                                key={product.setNumber}
                                                id={product.id}
                                                name={product.name}
                                                price={product.price}
                                                description={product.description}
                                                color={product.color}
                                                imgUrl={product.imgUrl}
                                                selectedProductsPage={true}
                                                deleteSelectedProduct={deleteSelectedProduct}
                                            />)}
            
        </div>
    )
}

export default Favorites